﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace UTADFINALL.Models
{
    [Table("TblHistoricoAluno2")]
    public partial class TblHistoricoAluno2
    {
        [Column("IDHISTORICOALUNO")]
        public int Idhistoricoaluno { get; set; }
        [Key]
        [Column("IDMATRICULA")]
        public int Idmatricula { get; set; }
        [Column("CODDISCIPLINA")]
        public int Coddisciplina { get; set; }
        [Column("CODOPCAO")]
        public int? Codopcao { get; set; }
        [Column("TIPOCODIFICACAO")]
        public int? Tipocodificacao { get; set; }
        [Required]
        [Column("NOTA")]
        [StringLength(10)]
        public string Nota { get; set; }
        [Column("TIPONOTA")]
        [StringLength(3)]
        public string Tiponota { get; set; }
        [Column("ANOLECTIVO")]
        public int Anolectivo { get; set; }
        [Column("DATA", TypeName = "datetime")]
        public DateTime Data { get; set; }
        [Column("CODEPOCAEXAME")]
        public byte Codepocaexame { get; set; }
        [Column("CREDITOS")]
        public double? Creditos { get; set; }
        [Column("IDASSRAMODISCIPLINA", TypeName = "numeric(18, 0)")]
        public decimal? Idassramodisciplina { get; set; }
        [Column("ECTS")]
        public double? Ects { get; set; }

        [ForeignKey(nameof(Idmatricula))]
        [InverseProperty(nameof(TblMatricula.TblHistoricoAluno2))]
        public virtual TblMatricula IdmatriculaNavigation { get; set; }
    }
}
