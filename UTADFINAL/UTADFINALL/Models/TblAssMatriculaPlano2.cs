﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace UTADFINALL.Models
{
    [Table("TblAssMatriculaPlano2")]
    public partial class TblAssMatriculaPlano2
    {
        [Key]
        [Column("IDASSMATRICULAPLANO")]
        public int Idassmatriculaplano { get; set; }
        [Column("IDMATRICULA")]
        public int? Idmatricula { get; set; }
        [Column("CODPLANO")]
        public int? Codplano { get; set; }
        [Column("ANOLECTIVO")]
        public short? Anolectivo { get; set; }
        [Column("DATAINICIO", TypeName = "smalldatetime")]
        public DateTime? Datainicio { get; set; }
        [Column("IDRAMO")]
        public int? Idramo { get; set; }

        [ForeignKey(nameof(Idmatricula))]
        [InverseProperty(nameof(TblMatricula.TblAssMatriculaPlano2s))]
        public virtual TblMatricula IdmatriculaNavigation { get; set; }
    }
}
