﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace UTADFINALL.Models
{
    [Table("TblAluno")]
    public partial class TblAluno
    {
        public TblAluno()
        {
            TblMatriculas = new HashSet<TblMatricula>();
        }

        [Key]
        [Column("NUMERO")]
        public int Numero { get; set; }
        [Column("NOME")]
        [StringLength(100)]
        public string Nome { get; set; }
        [Column("NOMEPROPRIO")]
        [StringLength(100)]
        public string Nomeproprio { get; set; }
        [Column("APELIDO")]
        [StringLength(100)]
        public string Apelido { get; set; }
        [Column("SEXO")]
        [StringLength(1)]
        public string Sexo { get; set; }
        [Column("DATANASC")]
        public DateTime? Datanasc { get; set; }
        [Column("PAI")]
        [StringLength(100)]
        public string Pai { get; set; }
        [Column("MAE")]
        [StringLength(100)]
        public string Mae { get; set; }
        [Column("NACIONALIDADE")]
        [StringLength(100)]
        public string Nacionalidade { get; set; }
        [Column("TIPOALUNO")]
        [StringLength(50)]
        public string Tipoaluno { get; set; }
        [Column("PASSWORD")]
        [StringLength(6)]
        public string Password { get; set; }
        [Column("EMAIL")]
        [StringLength(255)]
        public string Email { get; set; }
        [Column("OBERV")]
        [StringLength(255)]
        public string Oberv { get; set; }
        [Column("REGIME")]
        [StringLength(50)]
        public string Regime { get; set; }
        [Column("NIB")]
        [StringLength(21)]
        public string Nib { get; set; }
        [Column("COMOUTGOING")]
        public bool? Comoutgoing { get; set; }
        public bool? ComValidadeNotificacaoEmail { get; set; }

        [InverseProperty(nameof(TblMatricula.NumeroNavigation))]
        public virtual ICollection<TblMatricula> TblMatriculas { get; set; }
    }
}
