﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace UTADFINALL.Models
{
    [Table("TblCurso")]
    public partial class TblCurso
    {
        [Key]
        [Column("CODCURSO")]
        public int Codcurso { get; set; }
        [Column("DATACRIACAO", TypeName = "smalldatetime")]
        public DateTime? Datacriacao { get; set; }
        [Column("SIGLA")]
        [StringLength(6)]
        public string Sigla { get; set; }
        [Column("CODNACIONAL")]
        [StringLength(10)]
        public string Codnacional { get; set; }
        [Column("ESTADOACTUAL")]
        public int? Estadoactual { get; set; }
    }
}
