﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class Nome2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string procedures= @"Alter procedure  Nome( @num_mec int, @nome varchar(100) OUTPUT, @apelido varchar(100) OUTPUT )
as
Set @nome= (Select [NOMEPROPRIO] from TblAluno where [Numero]= @num_mec);
Set @apelido=(Select[APELIDO] from TblAluno where [Numero]= @num_mec);";
      migrationBuilder.Sql(procedures); 
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string procedure = @"Drop procedure Nome";
      migrationBuilder.Sql(procedure);
        }
    }
}
