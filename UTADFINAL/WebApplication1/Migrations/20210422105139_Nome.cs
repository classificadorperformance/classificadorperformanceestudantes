﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class Nome : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string procedure = @"Create procedure Nome( @num_mec int)
as
Declare @nome nvarchar(100);
Set @nome= (Select [NOME] from TblAluno where [Numero]= @num_mec);
return @nome; ";
      migrationBuilder.Sql(procedure);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string procedures = @"Drop procedures Nome";
      migrationBuilder.Sql(procedures);
        }
    }
}
