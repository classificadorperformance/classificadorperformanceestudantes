﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class SecondStep : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string procedure = @"
Create procedure Percentil_Curso( @num_mec int, @cod_curso int)
as

Declare @total int;
Declare @nota_menor int;
Declare @media float;
Declare @percentil int;

Set @media= (Select Media from TblMatricula where @num_mec= [NUMERO]);


Set @nota_menor= (Select Count(distinct TblMatricula.IDMATRICULA) from TblMatricula Inner Join TblMatriculaNormal ON TblMatricula.IDMATRICULA=TblMatriculaNormal.IDMATRICULA where @cod_curso= [CODCURSO] and [Media]<= @media)-1;
Set @total = (Select Count(distinct TblMatricula.IDMATRICULA) from TblMatricula Inner Join TblMatriculaNormal ON TblMatricula.IDMATRICULA=TblMatriculaNormal.IDMATRICULA where @cod_curso= [CODCURSO]);
Set @percentil = (@nota_menor*100)/(@total-1);
return @percentil; ";

      migrationBuilder.Sql(procedure);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string procedures = @"Drop procedure Percentil_Curso";
      migrationBuilder.Sql(procedures);
        }
    }
}
