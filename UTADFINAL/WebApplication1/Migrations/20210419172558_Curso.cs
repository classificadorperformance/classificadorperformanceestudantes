﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class Curso : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string procedures = @"Create Procedure Curso( @num_mec int)
as 
Declare @curso int
Declare @matricula int

Set @matricula= (Select [IDMATRICULA] from TblMatricula where [Numero]= @num_mec);
Set @curso = (Select [CODCURSO] from TblMatriculaNormal where @matricula= [IDMATRICULA]);
return @curso;";
      migrationBuilder.Sql(procedures);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string procedure = @"Drop Procedure Curso";
      migrationBuilder.Sql(procedure);
        }
    }
}
