﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class Nota : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string procedure = @"Create Procedure Nota (@num_mec int, @disciplina int)
as 
Declare @nota int;
Declare @string nvarchar(10);
Declare @id_matricula int;
Set @id_matricula= (Select [IDMATRICULA] from TblMatricula where [Numero]= @num_mec);
Set @string= (Select Nota from TblHistoricoAluno2 where [IDMATRICULA]= @id_matricula AND [CODDISCIPLINA]= @disciplina);
Set @nota= Cast(@string as int);
return @nota;";
      migrationBuilder.Sql(procedure);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string procedures = @"Drop Procedure Nota";
      migrationBuilder.Sql(procedures);
        }
    }
}
