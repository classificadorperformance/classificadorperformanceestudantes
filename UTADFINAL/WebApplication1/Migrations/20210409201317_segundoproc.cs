﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class segundoproc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string procedure = @"

Create procedure Percentil_Disciplinas( @num_mec int, @cod_disciplina int)
AS

Declare @total int;
Declare @nota_menor int;
Declare @nota nvarchar(10);
Declare @id_matricula int;
Declare @percentil int;

Set @id_matricula= (Select [IDMATRICULA] from TblMatricula where @num_mec=[NUMERO]);
Set @nota = (Select [NOTA] from TblHistoricoAluno2 where [IDMATRICULA]= @id_matricula and [CODDISCIPLINA]= @cod_disciplina);



Set @total= (Select Count(*) from TblHistoricoAluno2 where @cod_disciplina= [CODDISCIPLINA]);
Set @nota_menor= (Select Count(*) from TblHistoricoAluno2 where @cod_disciplina= [CODDISCIPLINA] and Cast([NOTA] AS int)<=Cast(@nota AS int))- 1;
Set @percentil= (@nota_menor*100)/(@total-1)

return @percentil";
      migrationBuilder.Sql(procedure);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string procedures = @"Drop procedure Percentil_Disciplinas";
      migrationBuilder.Sql(procedures);
        }
    }
}
