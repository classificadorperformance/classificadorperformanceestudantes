﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class Nomes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string procedure = @"Create procedure Nomes( @num_mec int, @nome varchar(100) OUTPUT )
as
Set @nome= (Select [NOME] from TblAluno where [Numero]= @num_mec);";
      migrationBuilder.Sql(procedure);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string procedures = @"Drop procedure Nomes";
      migrationBuilder.Sql(procedures);
        }
    }
}
