﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class MediaAlta : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string proc = @"Create procedure NotaAlta(@Disciplina int)
as 
Declare @notaalta int

Set @notaalta= (Select MAX(NOTA) from TblHistoricoAluno2 where @Disciplina=[CODDISCIPLINA])
return @notaalta;";

      migrationBuilder.Sql(proc);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string proc = @"Drop Procedure NotaAlta";
      migrationBuilder.Sql(proc);
        }
    }
}
