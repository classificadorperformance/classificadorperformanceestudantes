﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class FirstStep : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "TblAluno",
            //    columns: table => new
            //    {
            //        NUMERO = table.Column<int>(type: "int", nullable: false),
            //        NOME = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
            //        NOMEPROPRIO = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
            //        APELIDO = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
            //        SEXO = table.Column<string>(type: "nvarchar(1)", maxLength: 1, nullable: true),
            //        DATANASC = table.Column<DateTime>(type: "datetime2", nullable: true),
            //        PAI = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
            //        MAE = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
            //        NACIONALIDADE = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
            //        TIPOALUNO = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
            //        PASSWORD = table.Column<string>(type: "nvarchar(6)", maxLength: 6, nullable: true),
            //        EMAIL = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
            //        OBERV = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
            //        REGIME = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
            //        NIB = table.Column<string>(type: "nvarchar(21)", maxLength: 21, nullable: true),
            //        COMOUTGOING = table.Column<bool>(type: "bit", nullable: true),
            //        ComValidadeNotificacaoEmail = table.Column<bool>(type: "bit", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TblAluno", x => x.NUMERO);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TblCurso",
            //    columns: table => new
            //    {
            //        CODCURSO = table.Column<int>(type: "int", nullable: false),
            //        DATACRIACAO = table.Column<DateTime>(type: "smalldatetime", nullable: true),
            //        SIGLA = table.Column<string>(type: "nvarchar(6)", maxLength: 6, nullable: true),
            //        CODNACIONAL = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
            //        ESTADOACTUAL = table.Column<int>(type: "int", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TblCurso", x => x.CODCURSO);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TblCursoDescricao",
            //    columns: table => new
            //    {
            //        CODCURSO = table.Column<int>(type: "int", nullable: false),
            //        NOME = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
            //        DESCRICAO = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
            //        CODPAISIDIOMA = table.Column<short>(type: "smallint", nullable: true),
            //        DEP = table.Column<string>(type: "nvarchar(6)", maxLength: 6, nullable: true),
            //        DEP_NOVOANO = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
            //        estab = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
            //        DescGrau = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
            //        TipoFormacao = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
            //        IDTipoCurso = table.Column<int>(type: "int", nullable: true),
            //        NOMEING = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
            //        NOMEPARACARTAOESTUDANTE = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
            //        TIPOFORMACAO_rcu = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TblCursoDescricao", x => x.CODCURSO);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TblDisciplina2",
            //    columns: table => new
            //    {
            //        CODDISCIPLINA = table.Column<int>(type: "int", nullable: false),
            //        NOMEDISCIPLINA = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
            //        TIPODISCIPLINA = table.Column<short>(type: "smallint", nullable: true),
            //        DISCIPTEMP = table.Column<string>(type: "nvarchar(1)", maxLength: 1, nullable: true),
            //        CREDITOS = table.Column<int>(type: "int", nullable: true),
            //        SEMESTRE = table.Column<byte>(type: "tinyint", nullable: true),
            //        NOMEDISCIPLINAING = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
            //        TIPO_GENERICA = table.Column<short>(type: "smallint", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TblDisciplina2", x => x.CODDISCIPLINA);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TblPlanoCurricular2",
            //    columns: table => new
            //    {
            //        CODPLANO = table.Column<int>(type: "int", nullable: false),
            //        ETIQUETA = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
            //        DATACRIACAO = table.Column<DateTime>(type: "datetime", nullable: true),
            //        ESTADADOACTUAL = table.Column<short>(type: "smallint", nullable: true),
            //        TIPOPLANO = table.Column<short>(type: "smallint", nullable: true),
            //        CODCURSO = table.Column<int>(type: "int", nullable: true),
            //        CV = table.Column<string>(type: "nchar(2)", fixedLength: true, maxLength: 2, nullable: true),
            //        NOMERAMO = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
            //        NOMERAMOING = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TblPlanoCurricular2", x => x.CODPLANO);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TblPlanosCurr",
            //    columns: table => new
            //    {
            //        IDRAMO = table.Column<int>(type: "int", nullable: false),
            //        IDORD = table.Column<int>(type: "int", nullable: false),
            //        CODDISCIPLINA = table.Column<int>(type: "int", nullable: true),
            //        DISCIPLINA = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
            //        ANO = table.Column<int>(type: "int", nullable: true),
            //        SEMESTRE = table.Column<int>(type: "int", nullable: true),
            //        CREDITOS = table.Column<double>(type: "float", nullable: true),
            //        TIPODISCIP = table.Column<int>(type: "int", nullable: true),
            //        CODGRUPOP = table.Column<int>(type: "int", nullable: true),
            //        NOMEGRUPOP = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true),
            //        ESTADO = table.Column<short>(type: "smallint", nullable: true),
            //        DEP = table.Column<string>(type: "nvarchar(6)", maxLength: 6, nullable: true),
            //        TEORICAS = table.Column<double>(type: "float", nullable: true),
            //        TPRATICAS = table.Column<double>(type: "float", nullable: true),
            //        PRATICAS = table.Column<double>(type: "float", nullable: true),
            //        IDASSRAMODISCIPLINA = table.Column<decimal>(type: "numeric(18,0)", nullable: true),
            //        ECTS = table.Column<double>(type: "float", nullable: true),
            //        ECTS_1SEM = table.Column<double>(type: "float", nullable: true),
            //        ECTS_2SEM = table.Column<double>(type: "float", nullable: true),
            //        OT = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
            //        TC = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
            //        E = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
            //        S = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
            //        PL = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
            //        O = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
            //        TIPODISCIPLINA_GENERICA = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TblPlanosCurr", x => new { x.IDRAMO, x.IDORD });
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TblMatricula",
            //    columns: table => new
            //    {
            //        IDMATRICULA = table.Column<int>(type: "int", nullable: false),
            //        ANOLECTIVO = table.Column<short>(type: "smallint", nullable: false),
            //        DATAMATRICULA = table.Column<DateTime>(type: "smalldatetime", nullable: true),
            //        TIPOMATRICULA = table.Column<short>(type: "smallint", nullable: true),
            //        NUMERO = table.Column<int>(type: "int", nullable: true),
            //        ESTADOACTUAL = table.Column<short>(type: "smallint", nullable: true),
            //        ANOCURRICULAR = table.Column<short>(type: "smallint", nullable: true),
            //        REPGLOBAL = table.Column<bool>(type: "bit", nullable: true),
            //        ANO_PARA_INSCRICAO = table.Column<short>(type: "smallint", nullable: true),
            //        CURSO_CONCLUIDO = table.Column<bool>(type: "bit", nullable: true),
            //        FIM_CURSO_DEZEMBRO = table.Column<bool>(type: "bit", nullable: true),
            //        PLANO_BASE_CURR_A = table.Column<bool>(type: "bit", nullable: true),
            //        ETIQUETA_PLANO_BASE = table.Column<string>(type: "nchar(1)", fixedLength: true, maxLength: 1, nullable: true),
            //        UC_ATRASO = table.Column<double>(type: "float", nullable: true),
            //        DISCIP_1ANO_EM_ATRASO = table.Column<short>(type: "smallint", nullable: true),
            //        MEDIA = table.Column<double>(type: "float", nullable: true),
            //        ECTS_UC_PLANO_FEITOS = table.Column<double>(type: "float", nullable: true),
            //        ECTS_CREDITADOS = table.Column<double>(type: "float", nullable: true),
            //        NUM_DISCIP_CREDITADAS = table.Column<int>(type: "int", nullable: true),
            //        ALUNOUAMOB = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        ECTS_PLANO = table.Column<double>(type: "float", nullable: true),
            //        NUM_MATRICULAS = table.Column<int>(type: "int", nullable: true),
            //        FINALISTA = table.Column<bool>(type: "bit", nullable: true),
            //        NUM_DISCIP_PLANO_FEITAS = table.Column<int>(type: "int", nullable: true),
            //        NUM_DISCIP_PLANO_POR_FAZER = table.Column<int>(type: "int", nullable: true),
            //        IDMATRICULA_SIGACAD = table.Column<int>(type: "int", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__TblMatri__987508BCFAB05940", x => x.IDMATRICULA);
            //        table.ForeignKey(
            //            name: "FK__TblMatric__NUMER__25869641",
            //            column: x => x.NUMERO,
            //            principalTable: "TblAluno",
            //            principalColumn: "NUMERO",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TblAssMatriculaPlano2",
            //    columns: table => new
            //    {
            //        IDASSMATRICULAPLANO = table.Column<int>(type: "int", nullable: false),
            //        IDMATRICULA = table.Column<int>(type: "int", nullable: true),
            //        CODPLANO = table.Column<int>(type: "int", nullable: true),
            //        ANOLECTIVO = table.Column<short>(type: "smallint", nullable: true),
            //        DATAINICIO = table.Column<DateTime>(type: "smalldatetime", nullable: true),
            //        IDRAMO = table.Column<int>(type: "int", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TblAssMatriculaPlano2", x => x.IDASSMATRICULAPLANO);
            //        table.ForeignKey(
            //            name: "FK__TblAssMat__IDMAT__286302EC",
            //            column: x => x.IDMATRICULA,
            //            principalTable: "TblMatricula",
            //            principalColumn: "IDMATRICULA",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TblHistoricoAluno2",
            //    columns: table => new
            //    {
            //        IDMATRICULA = table.Column<int>(type: "int", nullable: false),
            //        IDHISTORICOALUNO = table.Column<int>(type: "int", nullable: false),
            //        CODDISCIPLINA = table.Column<int>(type: "int", nullable: false),
            //        CODOPCAO = table.Column<int>(type: "int", nullable: true),
            //        TIPOCODIFICACAO = table.Column<int>(type: "int", nullable: true),
            //        NOTA = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
            //        TIPONOTA = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: true),
            //        ANOLECTIVO = table.Column<int>(type: "int", nullable: false),
            //        DATA = table.Column<DateTime>(type: "datetime", nullable: false),
            //        CODEPOCAEXAME = table.Column<byte>(type: "tinyint", nullable: false),
            //        CREDITOS = table.Column<double>(type: "float", nullable: true),
            //        IDASSRAMODISCIPLINA = table.Column<decimal>(type: "numeric(18,0)", nullable: true),
            //        ECTS = table.Column<double>(type: "float", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__TblHisto__987508BC28E71875", x => x.IDMATRICULA);
            //        table.ForeignKey(
            //            name: "FK__TblHistor__IDMAT__34C8D9D1",
            //            column: x => x.IDMATRICULA,
            //            principalTable: "TblMatricula",
            //            principalColumn: "IDMATRICULA",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TblMatriculaNormal",
            //    columns: table => new
            //    {
            //        IDMATRICULA = table.Column<int>(type: "int", nullable: false),
            //        TIPOACESSO = table.Column<string>(type: "char(12)", unicode: false, fixedLength: true, maxLength: 12, nullable: true),
            //        CODCURSO = table.Column<int>(type: "int", nullable: true),
            //        CV = table.Column<string>(type: "char(3)", unicode: false, fixedLength: true, maxLength: 3, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.ForeignKey(
            //            name: "FK__TblMatric__IDMAT__32E0915F",
            //            column: x => x.IDMATRICULA,
            //            principalTable: "TblMatricula",
            //            principalColumn: "IDMATRICULA",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_TblAssMatriculaPlano2_IDMATRICULA",
            //    table: "TblAssMatriculaPlano2",
            //    column: "IDMATRICULA");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TblMatricula_NUMERO",
            //    table: "TblMatricula",
            //    column: "NUMERO");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TblMatriculaNormal_IDMATRICULA",
            //    table: "TblMatriculaNormal",
            //    column: "IDMATRICULA");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "TblAssMatriculaPlano2");

            //migrationBuilder.DropTable(
            //    name: "TblCurso");

            //migrationBuilder.DropTable(
            //    name: "TblCursoDescricao");

            //migrationBuilder.DropTable(
            //    name: "TblDisciplina2");

            //migrationBuilder.DropTable(
            //    name: "TblHistoricoAluno2");

            //migrationBuilder.DropTable(
            //    name: "TblMatriculaNormal");

            //migrationBuilder.DropTable(
            //    name: "TblPlanoCurricular2");

            //migrationBuilder.DropTable(
            //    name: "TblPlanosCurr");

            //migrationBuilder.DropTable(
            //    name: "TblMatricula");

            //migrationBuilder.DropTable(
            //    name: "TblAluno");
        }
    }
}
