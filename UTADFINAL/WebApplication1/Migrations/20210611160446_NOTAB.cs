﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class NOTAB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string proc = @"Alter procedure NotaBaixa(@Disciplina int)
as 
Declare @notabaixa int

Set @notabaixa= (Select MIN(NOTA) from TblHistoricoAluno2 where @Disciplina=[CODDISCIPLINA])
return @notabaixa;";
      migrationBuilder.Sql(proc);
    }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string proc = @"DROP Procedure NotaBaixa";
      migrationBuilder.Sql(proc);
        }
    }
}
