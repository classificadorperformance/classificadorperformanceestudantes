﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class Media : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string procedure = @"Create Procedure Media(@num_mec int)
as 
Declare @media int
Declare @matricula int

Set @matricula= (Select [IDMATRICULA] from TblMatricula where [Numero]= @num_mec);
Set @media = (Select Media from TblMatricula where @matricula= [IDMATRICULA]);
return @media;";
      migrationBuilder.Sql(procedure);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string procedures = @"Drop Procedure Media";
      migrationBuilder.Sql(procedures);
        }
    }
}
