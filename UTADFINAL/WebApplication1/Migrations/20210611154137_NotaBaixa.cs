﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class NotaBaixa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
      string proc = @"Create procedure NotaBaixa(@Disciplina int)
as 
Declare @notabaixa int

Set @notabaixa= (Select MAX(NOTA) from TblHistoricoAluno2 where @Disciplina=[CODDISCIPLINA])
return @notabaixa;";
      migrationBuilder.Sql(proc);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      string proc = @"Drop Procedure NotaBaixa";
      migrationBuilder.Sql(proc);

        }
    }
}
