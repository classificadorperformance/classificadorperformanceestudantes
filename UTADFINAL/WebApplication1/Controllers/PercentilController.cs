﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Data;
using WebApplication1.Models;
using System.Configuration;
using System.Data.SqlClient;


namespace WebApplication1.Controllers
{
    [Route("APIv1.0/Percentile")]
    [ApiController]
    public class PercentilController : ControllerBase
    {
        private readonly UtadContext _context;

        public PercentilController(UtadContext context)
        {
            _context = context;
        }

    // GET: api/Percentil
    [HttpGet]
    public async Task<ActionResult<IEnumerable<TblHistoricoAluno2>>> GetTblHistoricoAluno2s()
    {
      return await _context.TblHistoricoAluno2s.ToListAsync();
    }

    // GET: api/Percentil/5
    [HttpGet("{id}")]
    public async Task<ActionResult<TblHistoricoAluno2>> GetTblHistoricoAluno2(int id)
    {
      var tblHistoricoAluno2 = await _context.TblHistoricoAluno2s.FindAsync(id);

      if (tblHistoricoAluno2 == null)
      {
        return NotFound();
      }

      return tblHistoricoAluno2;
    }

    //// PUT: api/Percentil/5
    //// To protect from overposting attacks, enable the specific properties you want to bind to, for
    //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    //[HttpPut("{id}")]
    //public async Task<IActionResult> PutTblHistoricoAluno2(int id, TblHistoricoAluno2 tblHistoricoAluno2)
    //{
    //    if (id != tblHistoricoAluno2.Idmatricula)
    //    {
    //        return BadRequest();
    //    }

    //    _context.Entry(tblHistoricoAluno2).State = EntityState.Modified;

    //    try
    //    {
    //        await _context.SaveChangesAsync();
    //    }
    //    catch (DbUpdateConcurrencyException)
    //    {
    //        if (!TblHistoricoAluno2Exists(id))
    //        {
    //            return NotFound();
    //        }
    //        else
    //        {
    //            throw;
    //        }
    //    }

    //    return NoContent();
    //}

    //// POST: api/Percentil
    //// To protect from overposting attacks, enable the specific properties you want to bind to, for
    //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    //[HttpPost]
    //public async Task<ActionResult<TblHistoricoAluno2>> PostTblHistoricoAluno2(TblHistoricoAluno2 tblHistoricoAluno2)
    //{
    //    _context.TblHistoricoAluno2s.Add(tblHistoricoAluno2);
    //    try
    //    {
    //        await _context.SaveChangesAsync();
    //    }
    //    catch (DbUpdateException)
    //    {
    //        if (TblHistoricoAluno2Exists(tblHistoricoAluno2.Idmatricula))
    //        {
    //            return Conflict();
    //        }
    //        else
    //        {
    //            throw;
    //        }
    //    }

    //    return CreatedAtAction("GetTblHistoricoAluno2", new { id = tblHistoricoAluno2.Idmatricula }, tblHistoricoAluno2);
    //}

    //// DELETE: api/Percentil/5
    //[HttpDelete("{id}")]
    //public async Task<ActionResult<TblHistoricoAluno2>> DeleteTblHistoricoAluno2(int id)
    //{
    //    var tblHistoricoAluno2 = await _context.TblHistoricoAluno2s.FindAsync(id);
    //    if (tblHistoricoAluno2 == null)
    //    {
    //        return NotFound();
    //    }

    //    _context.TblHistoricoAluno2s.Remove(tblHistoricoAluno2);
    //    await _context.SaveChangesAsync();

    //    return tblHistoricoAluno2;
    //}


    //private bool TblHistoricoAluno2Exists(int id)
    //{
    //    return _context.TblHistoricoAluno2s.Any(e => e.Idmatricula == id);
    //}


    [HttpGet("{num}/{curso}")]
    public async Task<int> PercentilCurso(int num, int curso)
    {
      var numero = new SqlParameter("@numero", num);
      var cursos = new SqlParameter("@cursos", curso);

      var returnValue = new SqlParameter
      {
        ParameterName = "@procR",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var valor = _context.Database.ExecuteSqlRaw("exec @procR = [dbo].[Percentil_Curso] @numero, @cursos", numero, cursos, returnValue);
      int percentil = (int)returnValue.Value;
      
      string ok =   $"{percentil}" + $"{num}";
      return percentil;


      
      

    }

    [HttpGet("{num}/{disciplina}/1")]

    public async Task<int> PercentilDisciplina(int num, int disciplina)
    {
      var numero = new SqlParameter("@numero", num);
      var disciplinas = new SqlParameter("@disciplinas", disciplina);

      var returnValue = new SqlParameter
      {
        ParameterName = "@procR",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var valor = _context.Database.ExecuteSqlRaw("exec @procR = [dbo].[Percentil_Disciplinas] @numero, @disciplinas", numero, disciplinas, returnValue);
      int percentil = (int)returnValue.Value;
      return percentil;

    }

    [HttpGet("{num}/{disciplina}/2")]
    public IActionResult Json ( int num, int disciplina)
    {
      var numero = new SqlParameter("@numero", num);
      var disciplinas = new SqlParameter("@disciplinas", disciplina);

      var returnValue = new SqlParameter
      {
        ParameterName = "@procR",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var valor = _context.Database.ExecuteSqlRaw("exec @procR = [dbo].[Percentil_Disciplinas] @numero, @disciplinas", numero, disciplinas, returnValue);


      int percentil = (int)returnValue.Value;
      //string ok = $"{percentil}" + "Nome" + $"{num}";
      return new JsonResult(percentil);

      //[{ "idhistoricoaluno":1,"idmatricula":1,"coddisciplina":1133,"codopcao":50,"tipocodificacao":null}]

    }


    [HttpGet]
    [Route("Courses/{num}/{course}")]
    public IActionResult JsonCurso(int num, int course)
    {
      var numero = new SqlParameter("@numero", num);
      var cursos = new SqlParameter("@cursos", course);

      var returnValue = new SqlParameter
      {
        ParameterName = "@procR",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var valor = _context.Database.ExecuteSqlRaw("exec @procR = [dbo].[Percentil_Curso] @numero, @cursos", numero, cursos, returnValue);
      int percentil = (int)returnValue.Value;

      var returnMedia = new SqlParameter
      {
        ParameterName = "@procM",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var media = _context.Database.ExecuteSqlRaw("exec @procM = [dbo].[Media] @numero", numero, returnMedia);
      var valormedia = (int)returnMedia.Value;

      var returnNome = new SqlParameter
      {
        ParameterName = "procx",
        DbType = System.Data.DbType.String,
        Size = 100,
        Direction = ParameterDirection.Output,
        Value = "José"
      };

      var returnApelido = new SqlParameter
      {
        ParameterName = "procl",
        DbType = System.Data.DbType.String,
        Size = 100,
        Direction = ParameterDirection.Output,
        Value = "José"
      };



      var nome = _context.Database.ExecuteSqlRaw("exec Nome @numero, @procx out, @procl out", numero, returnNome, returnApelido);
      if(returnNome.Value==System.DBNull.Value)
      {
        return StatusCode(404);
      }
      var nomes = (string)returnNome.Value; // nome+ ultimo nome
      var apelidos = (string)returnApelido.Value;

      

      List<StudentCourseModel> json = new List<StudentCourseModel>()
      {
        new StudentCourseModel {Student_Number=num, Student= nomes+ " "+ apelidos, Course=course, Average=valormedia,Percentile=percentil }
      };
      return new JsonResult(json);

    }


    [HttpGet]
    [Route("CurricularUnits/{num}/{disciplina}")]
    public IActionResult JsonDisciplina(int num, int disciplina)
    {

      var numero = new SqlParameter("@numero", num);
      var disciplinas = new SqlParameter("@disciplinas", disciplina);

      var returnValue = new SqlParameter
      {
        ParameterName = "@procR",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var valor = _context.Database.ExecuteSqlRaw("exec @procR = [dbo].[Percentil_Disciplinas] @numero, @disciplinas", numero, disciplinas, returnValue);


      int percentil = (int)returnValue.Value;

      var returnCurso = new SqlParameter
      {
        ParameterName = "@procC",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var course= _context.Database.ExecuteSqlRaw("exec @procC = [dbo].[Curso] @numero", numero, returnCurso);
      var valorcurso = (int)returnCurso.Value;

      var returnMedia = new SqlParameter
      {
        ParameterName = "@procM",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var media = _context.Database.ExecuteSqlRaw("exec @procM = [dbo].[Media] @numero", numero, returnMedia);
      var valormedia = (int)returnMedia.Value;

      var returnNota = new SqlParameter
      {
        ParameterName = "@procN",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var nota = _context.Database.ExecuteSqlRaw("exec @procN = [dbo].[Nota] @numero, @disciplinas", numero,disciplinas, returnNota);
      var valornota = (int)returnNota.Value;

      var returnNome = new SqlParameter
      {
        ParameterName = "procx",
        DbType = System.Data.DbType.String,
        Size=100,
        Direction = ParameterDirection.Output,
        Value= "José"
      };

      var returnApelido = new SqlParameter
      {
        ParameterName = "procl",
        DbType = System.Data.DbType.String,
        Size = 100,
        Direction = ParameterDirection.Output,
        Value = "José"
      };

      var returnNotaAlta = new SqlParameter
      {
        ParameterName = "@procHG",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var highestgrade = _context.Database.ExecuteSqlRaw("exec @procHG = [dbo].[NotaAlta] @disciplinas", disciplinas, returnNotaAlta);
      var valorhg = (int)returnNotaAlta.Value;


      var returnNotaBaixa = new SqlParameter
      {
        ParameterName = "@procLG",
        SqlDbType = SqlDbType.Int,
        Direction = ParameterDirection.Output
      };

      var lowestgrade = _context.Database.ExecuteSqlRaw("exec @procLG = [dbo].[NotaBaixa] @disciplinas", disciplinas, returnNotaBaixa);
      var valorlg = (int)returnNotaBaixa.Value;

      var nome = _context.Database.ExecuteSqlRaw("exec Nome @numero, @procx out, @procl out", numero, returnNome, returnApelido);

      if (returnNome.Value == System.DBNull.Value)
      {
        return StatusCode(404);
      }
      var nomes = (string) returnNome.Value; // nome+ ultimo nome
      var apelidos = (string)returnApelido.Value; 




      List<StudentUCModel> json = new List<StudentUCModel>()
      {
        new StudentUCModel {Student_Number=num,Student=nomes+ " "+ apelidos , Course=valorcurso, CurricularUnit=disciplina,  Grade=valornota, Percentile=percentil, HighestGrade=valorhg, LowestGrade=valorlg}
      };
      return new JsonResult(json);

    }





    }
}
