﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebApplication1.Models;

#nullable disable

namespace WebApplication1.Data
{
    public partial class UtadContext : DbContext
    {
        public UtadContext()
        {
        }

        public UtadContext(DbContextOptions<UtadContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblAluno> TblAlunos { get; set; }
        public virtual DbSet<TblAssMatriculaPlano2> TblAssMatriculaPlano2s { get; set; }
        public virtual DbSet<TblCurso> TblCursos { get; set; }
        public virtual DbSet<TblCursoDescricao> TblCursoDescricaos { get; set; }
        public virtual DbSet<TblDisciplina2> TblDisciplina2s { get; set; }
        public virtual DbSet<TblHistoricoAluno2> TblHistoricoAluno2s { get; set; }
        public virtual DbSet<TblMatricula> TblMatriculas { get; set; }
        public virtual DbSet<TblMatriculaNormal> TblMatriculaNormals { get; set; }
        public virtual DbSet<TblPlanoCurricular2> TblPlanoCurricular2s { get; set; }
        public virtual DbSet<TblPlanosCurr> TblPlanosCurrs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("name=UtadContext");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<TblAluno>(entity =>
            {
                entity.Property(e => e.Numero).ValueGeneratedNever();
            });

            modelBuilder.Entity<TblAssMatriculaPlano2>(entity =>
            {
                entity.Property(e => e.Idassmatriculaplano).ValueGeneratedNever();

                entity.HasOne(d => d.IdmatriculaNavigation)
                    .WithMany(p => p.TblAssMatriculaPlano2s)
                    .HasForeignKey(d => d.Idmatricula)
                    .HasConstraintName("FK__TblAssMat__IDMAT__286302EC");
            });

            modelBuilder.Entity<TblCurso>(entity =>
            {
                entity.Property(e => e.Codcurso).ValueGeneratedNever();
            });

            modelBuilder.Entity<TblCursoDescricao>(entity =>
            {
                entity.Property(e => e.Codcurso).ValueGeneratedNever();
            });

            modelBuilder.Entity<TblDisciplina2>(entity =>
            {
                entity.Property(e => e.Coddisciplina).ValueGeneratedNever();
            });

            modelBuilder.Entity<TblHistoricoAluno2>(entity =>
            {
                entity.HasKey(e => e.Idmatricula)
                    .HasName("PK__TblHisto__987508BC28E71875");

                entity.Property(e => e.Idmatricula).ValueGeneratedNever();

                entity.HasOne(d => d.IdmatriculaNavigation)
                    .WithOne(p => p.TblHistoricoAluno2)
                    .HasForeignKey<TblHistoricoAluno2>(d => d.Idmatricula)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TblHistor__IDMAT__34C8D9D1");
            });

            modelBuilder.Entity<TblMatricula>(entity =>
            {
                entity.HasKey(e => e.Idmatricula)
                    .HasName("PK__TblMatri__987508BCFAB05940");

                entity.Property(e => e.Idmatricula).ValueGeneratedNever();

                entity.Property(e => e.Alunouamob).IsUnicode(false);

                entity.Property(e => e.EtiquetaPlanoBase).IsFixedLength(true);

                entity.HasOne(d => d.NumeroNavigation)
                    .WithMany(p => p.TblMatriculas)
                    .HasForeignKey(d => d.Numero)
                    .HasConstraintName("FK__TblMatric__NUMER__25869641");
            });

            modelBuilder.Entity<TblMatriculaNormal>(entity =>
            {
                entity.Property(e => e.Cv)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Tipoacesso)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.HasOne(d => d.IdmatriculaNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.Idmatricula)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TblMatric__IDMAT__32E0915F");
            });

            modelBuilder.Entity<TblPlanoCurricular2>(entity =>
            {
                entity.Property(e => e.Codplano).ValueGeneratedNever();

                entity.Property(e => e.Cv).IsFixedLength(true);
            });

            modelBuilder.Entity<TblPlanosCurr>(entity =>
            {
                entity.HasKey(e => new { e.Idramo, e.Idord });

                entity.Property(e => e.E).IsFixedLength(true);

                entity.Property(e => e.O).IsFixedLength(true);

                entity.Property(e => e.Ot).IsFixedLength(true);

                entity.Property(e => e.Pl).IsFixedLength(true);

                entity.Property(e => e.S).IsFixedLength(true);

                entity.Property(e => e.Tc).IsFixedLength(true);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
