﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebApplication1.Models
{
    [Table("TblPlanoCurricular2")]
    public partial class TblPlanoCurricular2
    {
        [Key]
        [Column("CODPLANO")]
        public int Codplano { get; set; }
        [Column("ETIQUETA")]
        [StringLength(255)]
        public string Etiqueta { get; set; }
        [Column("DATACRIACAO", TypeName = "datetime")]
        public DateTime? Datacriacao { get; set; }
        [Column("ESTADADOACTUAL")]
        public short? Estadadoactual { get; set; }
        [Column("TIPOPLANO")]
        public short? Tipoplano { get; set; }
        [Column("CODCURSO")]
        public int? Codcurso { get; set; }
        [Column("CV")]
        [StringLength(2)]
        public string Cv { get; set; }
        [Column("NOMERAMO")]
        [StringLength(100)]
        public string Nomeramo { get; set; }
        [Column("NOMERAMOING")]
        [StringLength(250)]
        public string Nomeramoing { get; set; }
    }
}
