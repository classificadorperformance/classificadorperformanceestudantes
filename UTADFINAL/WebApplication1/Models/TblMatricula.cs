﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebApplication1.Models
{
    [Table("TblMatricula")]
    public partial class TblMatricula
    {
        public TblMatricula()
        {
            TblAssMatriculaPlano2s = new HashSet<TblAssMatriculaPlano2>();
        }

        [Column("ANOLECTIVO")]
        public short Anolectivo { get; set; }
        [Column("DATAMATRICULA", TypeName = "smalldatetime")]
        public DateTime? Datamatricula { get; set; }
        [Column("TIPOMATRICULA")]
        public short? Tipomatricula { get; set; }
        [Key]
        [Column("IDMATRICULA")]
        public int Idmatricula { get; set; }
        [Column("NUMERO")]
        public int? Numero { get; set; }
        [Column("ESTADOACTUAL")]
        public short? Estadoactual { get; set; }
        [Column("ANOCURRICULAR")]
        public short? Anocurricular { get; set; }
        [Column("REPGLOBAL")]
        public bool? Repglobal { get; set; }
        [Column("ANO_PARA_INSCRICAO")]
        public short? AnoParaInscricao { get; set; }
        [Column("CURSO_CONCLUIDO")]
        public bool? CursoConcluido { get; set; }
        [Column("FIM_CURSO_DEZEMBRO")]
        public bool? FimCursoDezembro { get; set; }
        [Column("PLANO_BASE_CURR_A")]
        public bool? PlanoBaseCurrA { get; set; }
        [Column("ETIQUETA_PLANO_BASE")]
        [StringLength(1)]
        public string EtiquetaPlanoBase { get; set; }
        [Column("UC_ATRASO")]
        public double? UcAtraso { get; set; }
        [Column("DISCIP_1ANO_EM_ATRASO")]
        public short? Discip1anoEmAtraso { get; set; }
        [Column("MEDIA")]
        public double? Media { get; set; }
        [Column("ECTS_UC_PLANO_FEITOS")]
        public double? EctsUcPlanoFeitos { get; set; }
        [Column("ECTS_CREDITADOS")]
        public double? EctsCreditados { get; set; }
        [Column("NUM_DISCIP_CREDITADAS")]
        public int? NumDiscipCreditadas { get; set; }
        [Column("ALUNOUAMOB")]
        [StringLength(50)]
        public string Alunouamob { get; set; }
        [Column("ECTS_PLANO")]
        public double? EctsPlano { get; set; }
        [Column("NUM_MATRICULAS")]
        public int? NumMatriculas { get; set; }
        [Column("FINALISTA")]
        public bool? Finalista { get; set; }
        [Column("NUM_DISCIP_PLANO_FEITAS")]
        public int? NumDiscipPlanoFeitas { get; set; }
        [Column("NUM_DISCIP_PLANO_POR_FAZER")]
        public int? NumDiscipPlanoPorFazer { get; set; }
        [Column("IDMATRICULA_SIGACAD")]
        public int? IdmatriculaSigacad { get; set; }

        [ForeignKey(nameof(Numero))]
        [InverseProperty(nameof(TblAluno.TblMatriculas))]
        public virtual TblAluno NumeroNavigation { get; set; }
        [InverseProperty("IdmatriculaNavigation")]
        public virtual TblHistoricoAluno2 TblHistoricoAluno2 { get; set; }
        [InverseProperty(nameof(TblAssMatriculaPlano2.IdmatriculaNavigation))]
        public virtual ICollection<TblAssMatriculaPlano2> TblAssMatriculaPlano2s { get; set; }
    }
}
