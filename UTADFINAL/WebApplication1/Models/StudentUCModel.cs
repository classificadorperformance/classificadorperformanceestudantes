﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
  public class StudentUCModel
  {
    public string Student { get; set; }
    public int Student_Number { get; set; }
    public int Course { get; set; }
    public int Grade { get; set; }
    public int CurricularUnit { get; set; }
    public int Percentile { get; set; }

    public int HighestGrade { get; set; }
    public int LowestGrade { get; set; }

  }
}
