﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebApplication1.Models
{
    [Keyless]
    [Table("TblMatriculaNormal")]
    public partial class TblMatriculaNormal
    {
        [Column("IDMATRICULA")]
        public int Idmatricula { get; set; }
        [Column("TIPOACESSO")]
        [StringLength(12)]
        public string Tipoacesso { get; set; }
        [Column("CODCURSO")]
        public int? Codcurso { get; set; }
        [Column("CV")]
        [StringLength(3)]
        public string Cv { get; set; }

        [ForeignKey(nameof(Idmatricula))]
        public virtual TblMatricula IdmatriculaNavigation { get; set; }
    }
}
