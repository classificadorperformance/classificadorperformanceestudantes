﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebApplication1.Models
{
    [Table("TblDisciplina2")]
    public partial class TblDisciplina2
    {
        [Key]
        [Column("CODDISCIPLINA")]
        public int Coddisciplina { get; set; }
        [Column("NOMEDISCIPLINA")]
        [StringLength(300)]
        public string Nomedisciplina { get; set; }
        [Column("TIPODISCIPLINA")]
        public short? Tipodisciplina { get; set; }
        [Column("DISCIPTEMP")]
        [StringLength(1)]
        public string Disciptemp { get; set; }
        [Column("CREDITOS")]
        public int? Creditos { get; set; }
        [Column("SEMESTRE")]
        public byte? Semestre { get; set; }
        [Column("NOMEDISCIPLINAING")]
        [StringLength(300)]
        public string Nomedisciplinaing { get; set; }
        [Column("TIPO_GENERICA")]
        public short? TipoGenerica { get; set; }
    }
}
