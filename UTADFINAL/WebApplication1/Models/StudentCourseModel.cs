﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
  public class StudentCourseModel
  {


    public string Student { get; set; }
    public int Student_Number { get; set; }
    public int Course { get; set; }
    public int Average { get; set; }
    public int Percentile { get; set; }
  }
}
