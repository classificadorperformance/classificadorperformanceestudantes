﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebApplication1.Models
{
    [Table("TblCursoDescricao")]
    public partial class TblCursoDescricao
    {
        [Required]
        [Column("NOME")]
        [StringLength(250)]
        public string Nome { get; set; }
        [Column("DESCRICAO")]
        [StringLength(250)]
        public string Descricao { get; set; }
        [Key]
        [Column("CODCURSO")]
        public int Codcurso { get; set; }
        [Column("CODPAISIDIOMA")]
        public short? Codpaisidioma { get; set; }
        [Column("DEP")]
        [StringLength(6)]
        public string Dep { get; set; }
        [Column("DEP_NOVOANO")]
        [StringLength(50)]
        public string DepNovoano { get; set; }
        [Column("estab")]
        [StringLength(5)]
        public string Estab { get; set; }
        [StringLength(50)]
        public string DescGrau { get; set; }
        [StringLength(10)]
        public string TipoFormacao { get; set; }
        [Column("IDTipoCurso")]
        public int? IdtipoCurso { get; set; }
        [Column("NOMEING")]
        [StringLength(250)]
        public string Nomeing { get; set; }
        [Column("NOMEPARACARTAOESTUDANTE")]
        [StringLength(250)]
        public string Nomeparacartaoestudante { get; set; }
        [Column("TIPOFORMACAO_rcu")]
        [StringLength(50)]
        public string TipoformacaoRcu { get; set; }
    }
}
