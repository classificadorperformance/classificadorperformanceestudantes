﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
  public class AlunoCursoModel
  {
    public string Nome { get; set; }
    public int Num_mec { get; set; }
    public int Curso { get; set; }
    public int Media { get; set; }
    public int Percentil { get; set; }
  }
}
