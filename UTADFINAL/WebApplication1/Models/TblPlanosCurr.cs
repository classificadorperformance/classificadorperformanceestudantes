﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebApplication1.Models
{
    [Table("TblPlanosCurr")]
    public partial class TblPlanosCurr
    {
        [Key]
        [Column("IDRAMO")]
        public int Idramo { get; set; }
        [Key]
        [Column("IDORD")]
        public int Idord { get; set; }
        [Column("CODDISCIPLINA")]
        public int? Coddisciplina { get; set; }
        [Column("DISCIPLINA")]
        [StringLength(255)]
        public string Disciplina { get; set; }
        [Column("ANO")]
        public int? Ano { get; set; }
        [Column("SEMESTRE")]
        public int? Semestre { get; set; }
        [Column("CREDITOS")]
        public double? Creditos { get; set; }
        [Column("TIPODISCIP")]
        public int? Tipodiscip { get; set; }
        [Column("CODGRUPOP")]
        public int? Codgrupop { get; set; }
        [Column("NOMEGRUPOP")]
        [StringLength(60)]
        public string Nomegrupop { get; set; }
        [Column("ESTADO")]
        public short? Estado { get; set; }
        [Column("DEP")]
        [StringLength(6)]
        public string Dep { get; set; }
        [Column("TEORICAS")]
        public double? Teoricas { get; set; }
        [Column("TPRATICAS")]
        public double? Tpraticas { get; set; }
        [Column("PRATICAS")]
        public double? Praticas { get; set; }
        [Column("IDASSRAMODISCIPLINA", TypeName = "numeric(18, 0)")]
        public decimal? Idassramodisciplina { get; set; }
        [Column("ECTS")]
        public double? Ects { get; set; }
        [Column("ECTS_1SEM")]
        public double? Ects1sem { get; set; }
        [Column("ECTS_2SEM")]
        public double? Ects2sem { get; set; }
        [Column("OT")]
        [StringLength(10)]
        public string Ot { get; set; }
        [Column("TC")]
        [StringLength(10)]
        public string Tc { get; set; }
        [StringLength(10)]
        public string E { get; set; }
        [StringLength(10)]
        public string S { get; set; }
        [Column("PL")]
        [StringLength(10)]
        public string Pl { get; set; }
        [StringLength(10)]
        public string O { get; set; }
        [Column("TIPODISCIPLINA_GENERICA")]
        [StringLength(50)]
        public string TipodisciplinaGenerica { get; set; }
    }
}
