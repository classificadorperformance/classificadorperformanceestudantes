import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math

import sklearn
from sklearn import neighbors
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split


dataset = pd.read_csv("C:/DatasetV2.csv",header =0, sep = ';')
mat = dataset.to_numpy()
mat = mat[:,1:-4]
#print(mat)

media = []
#print(mat.shape[0])

for i in range (mat.shape[0]):
    m = sum(mat[i])/mat.shape[1]
    media.append(m)
print(media)

aluno = [0,1,2,3,4,5,8,9,10]
notas = [10,11,12,13,14,16,17,18,20]
m_notas = (np.reshape(notas,(1,len(aluno))))

matr = mat[:,aluno[0]]

for i in range (1,len(aluno)):
    matr = np.hstack((matr, mat[:,aluno[i]]))
print(matr)


# INICIO KNN

# Create feature and target arrays
X = matr
X= np.reshape(matr,(100,len(aluno)))
y = media


# Split into training and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=42)


rmse_val = [] #to store rmse values for different k
#for K in range(20):
#    K = K+1
model = neighbors.KNeighborsRegressor(n_neighbors = 1)

model.fit(X_train, y_train)  #fit the model
pred=model.predict(X_test) #make prediction on test set
error = math.sqrt(mean_squared_error(y_test,pred)) #calculate rmse
rmse_val.append(error) #store rmse values
print('RMSE value for k= ' , 1, 'is:', error)
model.fit(X, y)  #fit the model
nota = model.predict(m_notas)
print(nota)


